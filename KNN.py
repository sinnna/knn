import pandas as pd
from numpy import linalg as LA
import numpy as np
import csv

NUM_DATA = 10000
NUM_ROWS = 182
NUM_TEST = 3500
k = 10

def distance(a, b):
	return LA.norm(a - b)

def d1(a, b):
	return np.max(a - b)

def d2(a, b):
	return np.sum(abs(a - b))

data_train = np.array(pd.read_csv('Fashion_MNIST/Train_Data.csv', header=None))
label_train = np.array(pd.read_csv('Fashion_MNIST/Train_Labels.csv', header=None))
data_test = np.array(pd.read_csv('Fashion_MNIST/Test_Data.csv', header=None))
label_test = np.array(pd.read_csv('Fashion_MNIST/Test_Labels.csv', header=None))

results = []
correct = 0.0
for j in range(NUM_TEST):
	res = [0] * NUM_DATA
	for i in range(NUM_DATA):
		# res[i] = distance(data_test[j:j+1], data_train[i : i + 1])
		# res[i] = d1(data_test[j:j+1], data_train[i : i + 1])
		res[i] = d2(data_test[j:j+1], data_train[i : i + 1])		
	
	# print("@@", res)
	# print(res)

	idx = np.argpartition(res, k)[:k]
	a = {}
	for i in label_train[idx[0 : k]]:
		b = int(i)
		if(b not in a):
			a[b] = 1
		else:
			a[b] = a[b] + 1
	
	itemMaxValue = max(a.items(), key=lambda x : x[1])
	results.append(itemMaxValue[0])
	print("test label : ", label_test[j], "vs guessed : ", results[j])
	if(label_test[j] == results[j]):
		correct = correct + 1

print("accuracy : ",correct / NUM_TEST)
